pip install -r requirements.txt

python -u run.py \
    --do_train \
    --do_eval \
    --model_type roberta \
    --model_name_or_path SEBIS/code_trans_t5_base_code_documentation_generation_java_multitask \
    --load_model_path ../models/best_ppl_pretraining_pytorch.bin \
    --train_filename ../data/train.json \
    --dev_filename ../data/eval.json \
    --output_dir ../models \
    --max_source_length 128 \
    --max_target_length 128 \
    --beam_size 10 \
    --num_return_sequence 10 \
    --length_penalty 1 \
    --train_batch_size 32 \
    --eval_batch_size 32 \
    --learning_rate 1e-3 \
    --train_steps 28000 \
    --eval_steps 2000

python -u run.py \
    --do_test \
    --model_type roberta \
    --model_name_or_path SEBIS/code_trans_t5_base_code_documentation_generation_java_multitask \
    --load_model_path ../models/best_bleu_finetuning_pytorch.bin \
    --test_filename ../data/test.json \
    --output_dir ../models \
    --max_source_length 128 \
    --max_target_length 128 \
    --beam_size 10 \
    --num_return_sequence 10 \
    --length_penalty 1 \
    --train_batch_size 32 \
    --eval_batch_size 32 \
    --learning_rate 1e-3 \
    --train_steps 40000 \
    --eval_steps 8000
# AUGER FSE-2022

This is the public data of AUGER, the [paper](https://dl.acm.org/doi/abs/10.1145/3540250.3549099) "AUGER: Automatically Generating Review Comments with Pre-training Models" accepted by ESEC/FES 2022.

## Data

The [data](https://gitlab.com/ai-for-se-public-data/auger-fse-2022/-/tree/main/data) directory contains data files which are used for AUGER

- 100samples_case_study.json
- eval.json
- prediction.json
- raw_data.json
- test.json
- train.json


## AUGER

### Code
The [code](https://gitlab.com/ai-for-se-public-data/auger-fse-2022/-/tree/main/AUGER/code) directory contains code files to run the models and reproduce AUGER's results in the paper.

- requirements.txt
- run.py
- run.sh

### Model
We preserve the original models AUGER trained including:

- pre-training model (The best one in PPL)
- fine-tuning model (The best one in BLEU)

The model directory is empty due to the limitation of file size. The models can be loaded in [here](https://figshare.com/articles/dataset/models_of_AUGER/20375592).

## Environment

The AUGER was trained with:

- Python >= 3.7.10
- Pytorch >= 1.10.1
- Cuda >= 11.3


## Authors and acknowledgment

### Author: Li Lingwei

### Acknowledgement:
We sincerely appreciate the valuable feedback from the anonymous ESEC/FSE’2022 reviewers. This work was supported by the National Key R\&D Program of China (No.2021YFC3340204), the Strategy Priority Research Program of Chinese Academy of Sciences (No.XDA20080200) and Chinese Academy of Sciences-Dongguan Science and Technology Service Network Plan (No. 202016002000032).

<!--
## License
This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Permission to make digital or hard copies of part or all of this work for personal or classroom use is granted without fee provided that copies are not made or distributed for profit or commercial advantage and that copies bear this notice and the full citation on the first page. Copyrights for third-party components of this work must be honored.
For all other uses, contact us.
-->
